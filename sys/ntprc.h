typedef enum _SYSTEMINFOCLASS {
	SystemBasicInfo,        					//0
	SystemProcessorInfo,      				//1
	SystemPerformanceInfo,		    		//2
	SystemTimeInfo,						        //3
	SystemPathInfo,						        //4
	SystemProcessThreadInfo,			    //5
	SystemServiceDescriptorTableInfo,	//6
	SystemIoConfigInfo,					      //7
	SystemProcessorTimeInfo,			    //8
	SystemNtGlobalFlagInfo,				    //9
	SystemNotImplemented1,				    //10
	SystemModuleInfo,					        //11
	SystemResourceLockInfo,				    //12
	SystemNotImplemented2,				    //13
	SystemNotImplemented3,				    //14
	SystemNotImplemented4,				    //15
	SystemHandleInfo,					        //16
	SystemObjectInformation,			    //17
	SystemPageFileInformation,			  //18
	SystemInstructionEmulationInfo,		//19
	SystemNotUsed1,					        	//20
	SystemCacheInfo,					        //21
	SystemPoolTagInfo,					      //22
	SystemProcessorScheduleInfo,		  //23
	SystemDpcInfo,						        //24
	SystemNotImplemented5,				    //25
	SystemLoadSystemImage,				    //26
	SystemUnloadSystemImage,			    //27
	SystemTimerInfo,					        //28
	SystemNotImplemented6,				    //29
	SystemNotImplemented7,				    //30
	SystemNotImplemented8,				    //31
	SystemCrashDumpSectionInfo,			  //32
	SystemProcessorFaultCountInfo,		//33
	SystemCrashDumpStateInfo,			    //34
	SystemDebuggerInfo,					      //35
	SystemThreadSwitchCounters,			  //36
	SystemQuotaInfo,					        //37
	SystemLoadDriverInfo,				      //38
	SystemPrioritySeparation,			    //39
	SystemNotImplemented9,				    //40
	SystemNotImplemented10,				    //41
	SystemNotUsed6,						        //42
	SystemNotUsed7,						        //43
	SystemTimeZoneInfo,					      //44
	SystemLookasideInfo,				      //45
} SYSTEMINFOCLASS;




typedef NTSTATUS (*NTQUERYSYSTEMINFORMATION)(IN SYSTEMINFOCLASS SystemInfoClass, OUT PVOID SystemInfoBuffer, IN ULONG SystemInfoBufferSize, OUT PULONG BytesReturned OPTIONAL);
NTSYSAPI NTSTATUS NTAPI ZwQuerySystemInformation(IN SYSTEMINFOCLASS SystemInfoClass, OUT PVOID SystemInfoBuffer, IN ULONG SystemInfoBufferSize, OUT PULONG BytesReturned OPTIONAL);




NTQUERYSYSTEMINFORMATION OldNtQuerySystemInformation;

struct _SYSTEM_THREADS
{
  LARGE_INTEGER             KernelTime;
	LARGE_INTEGER             UserTime;
	LARGE_INTEGER             CreateTime;
	ULONG                     WaitTime;
	PVOID                     StartAddress;
	CLIENT_ID                 ClientIs;
	KPRIORITY                 Priority;
	KPRIORITY                 BasePriority;
	ULONG                     ContextSwitchCount;
	ULONG                     ThreadState;
	KWAIT_REASON              WaitReason;
};

struct _SYSTEM_PROCESSES
{
	ULONG                     NextEntryDelta;
	ULONG                     ThreadCount;
	ULONG                     Reserved[6];
	LARGE_INTEGER             CreateTime;
	LARGE_INTEGER             UserTime;
	LARGE_INTEGER             KernelTime;
	UNICODE_STRING            ProcessName;
	KPRIORITY                 BasePriority;
	ULONG                     ProcessId;
	ULONG                     InheritedFromProcessId;
	ULONG                     HandleCount;
	ULONG                     Reserved2[2];
	VM_COUNTERS               VmCounters;
	IO_COUNTERS               IoCounters;
	struct _SYSTEM_THREADS    Threads[1];
};


