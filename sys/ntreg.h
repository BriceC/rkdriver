
typedef NTSTATUS (*NTENUMERATEVALUEKEY)(IN HANDLE KeyHandle,IN ULONG Index,IN KEY_VALUE_INFORMATION_CLASS KeyValueInformationClass,OUT PVOID KeyValueInformation,IN ULONG Length,OUT PULONG ResultLength );
NTSYSAPI NTSTATUS NTAPI ZwEnumerateValueKey(IN HANDLE KeyHandle,IN ULONG Index,IN KEY_VALUE_INFORMATION_CLASS KeyValueInformationClass,OUT PVOID KeyValueInformation,IN ULONG Length,OUT PULONG ResultLength );


NTSYSAPI NTSTATUS NTAPI ZwEnumerateKey(IN HANDLE KeyHandle, IN ULONG Index, IN KEY_INFORMATION_CLASS KeyInformationClass, OUT PVOID KeyInformation, IN ULONG Length, OUT PULONG ResultLength ); 
typedef NTSTATUS (*NTENUMERATEKEY)(IN HANDLE KeyHandle, IN ULONG Index, IN KEY_INFORMATION_CLASS KeyInformationClass, OUT PVOID KeyInformation, IN ULONG Length, OUT PULONG ResultLength ); 






NTENUMERATEVALUEKEY OldZwEnumerateValueKey;
NTENUMERATEKEY OldZwEnumerateKey;
