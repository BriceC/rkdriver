#include "fct.h"

typedef struct _hiddens{
	char name[255];
}hiddens;

hiddens hidden[8];

void LoadHidden(){
	strcpy(hidden[0].name,"IceSword");
	strcpy(hidden[1].name,"_dt");
	strcpy(hidden[2].name,"rootkit");
	strcpy(hidden[3].name,"Rootkit");
	strcpy(hidden[4].name,"catchme");
	strcpy(hidden[5].name,"fsbl");
	strcpy(hidden[6].name,"radix");
	strcpy(hidden[7].name,"Tcp");

}

int IsHidden(char string1[]){
	int i=0;
	for (i=0;i<=6;i++){
		if(strstr2(string1,hidden[i].name)){
			return 1;
		}
	}
	return 0;
}

char *strstr2(char string1[], char string2[])
{
    char *start, *p1, *p2;
    for(start = &string1[0]; *start != '\0'; start++)
        {        /* for each position in input string... */
        p1 = string2;    /* prepare to check for pattern string there */
        p2 = start;
        while(*p1 != '\0')
            {
            if(*p1 != *p2)    /* characters differ */
                break;
            p1++;
            p2++;
            }
        if(*p1 == '\0')        /* found match */
            return start;
        }

    return 0;

    }

char *strstr3(char string1[], char string2[],int size)
{
    char *start, *p1, *p2;
    for(start = &string1[0]; size--; start++)
        {        /* for each position in input string... */
        p1 = string2;    /* prepare to check for pattern string there */
        p2 = start;
        while(size)
            {
            if(*p1 != *p2)    /* characters differ */
                break;
            p1++;
            p2++;
            }
        if(!size)        /* found match */
            return start;
        }

    return 0;

    }