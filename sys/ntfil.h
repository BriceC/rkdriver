#define DWORD unsigned long
#define WORD unsigned short
#define BOOL unsigned long
typedef unsigned char *LPBYTE;


typedef struct _FILE_DIRECTORY_INFORMATION {
    ULONG NextEntryOffset;
    ULONG FileIndex;
    LARGE_INTEGER CreationTime;
    LARGE_INTEGER LastAccessTime;
    LARGE_INTEGER LastWriteTime;
    LARGE_INTEGER ChangeTime;
    LARGE_INTEGER EndOfFile;
    LARGE_INTEGER AllocationSize;
    ULONG FileAttributes;
    ULONG FileNameLength;
    WCHAR FileName[1];
} FILE_DIRECTORY_INFORMATION, *PFILE_DIRECTORY_INFORMATION;

typedef struct _FILE_FULL_DIR_INFORMATION {
    ULONG NextEntryOffset;
    ULONG FileIndex;
    LARGE_INTEGER CreationTime;
    LARGE_INTEGER LastAccessTime;
    LARGE_INTEGER LastWriteTime;
    LARGE_INTEGER ChangeTime;
    LARGE_INTEGER EndOfFile;
    LARGE_INTEGER AllocationSize;
    ULONG FileAttributes;
    ULONG FileNameLength;
    ULONG EaSize;
    WCHAR FileName[1];
} FILE_FULL_DIR_INFORMATION, *PFILE_FULL_DIR_INFORMATION;

typedef struct _FILE_ID_FULL_DIR_INFORMATION {
    ULONG NextEntryOffset;
    ULONG FileIndex;
    LARGE_INTEGER CreationTime;
    LARGE_INTEGER LastAccessTime;
    LARGE_INTEGER LastWriteTime;
    LARGE_INTEGER ChangeTime;
    LARGE_INTEGER EndOfFile;
    LARGE_INTEGER AllocationSize;
    ULONG FileAttributes;
    ULONG FileNameLength;
    ULONG EaSize;
    LARGE_INTEGER FileId;
    WCHAR FileName[1];
} FILE_ID_FULL_DIR_INFORMATION, *PFILE_ID_FULL_DIR_INFORMATION;

typedef struct _FILE_BOTH_DIR_INFORMATION {
    ULONG NextEntryOffset;
    ULONG FileIndex;
    LARGE_INTEGER CreationTime;
    LARGE_INTEGER LastAccessTime;
    LARGE_INTEGER LastWriteTime;
    LARGE_INTEGER ChangeTime;
    LARGE_INTEGER EndOfFile;
    LARGE_INTEGER AllocationSize;
    ULONG FileAttributes;
    ULONG FileNameLength;
    ULONG EaSize;
    CCHAR ShortNameLength;
    WCHAR ShortName[12];
    WCHAR FileName[1];
} FILE_BOTH_DIR_INFORMATION, *PFILE_BOTH_DIR_INFORMATION;

typedef struct _FILE_ID_BOTH_DIR_INFORMATION {
    ULONG NextEntryOffset;
    ULONG FileIndex;
    LARGE_INTEGER CreationTime;
    LARGE_INTEGER LastAccessTime;
    LARGE_INTEGER LastWriteTime;
    LARGE_INTEGER ChangeTime;
    LARGE_INTEGER EndOfFile;
    LARGE_INTEGER AllocationSize;
    ULONG FileAttributes;
    ULONG FileNameLength;
    ULONG EaSize;
    CCHAR ShortNameLength;
    WCHAR ShortName[12];
    LARGE_INTEGER FileId;
    WCHAR FileName[1];
} FILE_ID_BOTH_DIR_INFORMATION, *PFILE_ID_BOTH_DIR_INFORMATION;

typedef struct _FILE_NAMES_INFORMATION {
    ULONG NextEntryOffset;
    ULONG FileIndex;
    ULONG FileNameLength;
    WCHAR FileName[1];
} FILE_NAMES_INFORMATION, *PFILE_NAMES_INFORMATION;




typedef struct _FILE_STREAM_INFORMATION { // Information Class 22
    ULONG NextEntryOffset;
    ULONG StreamNameLength;
    LARGE_INTEGER EndOfStream;
    LARGE_INTEGER AllocationSize;
    WCHAR StreamName[1];
} FILE_STREAM_INFORMATION, *PFILE_STREAM_INFORMATION;


NTSYSAPI NTSTATUS NTAPI ZwQueryDirectoryFile(IN HANDLE hFile,
                                             IN HANDLE hEvent OPTIONAL,
                                             IN PIO_APC_ROUTINE IoApcRoutine OPTIONAL,
                                             IN PVOID IoApcContext OPTIONAL,
                                             OUT PIO_STATUS_BLOCK pIoStatusBlock,
                                             OUT PVOID FileInformationBuffer,
                                             IN ULONG FileInformationBufferLength,
                                             IN FILE_INFORMATION_CLASS FileInfoClass,
                                             IN BOOLEAN bReturnOnlyOneEntry,
                                             IN PUNICODE_STRING PathMask OPTIONAL,
                                             IN BOOLEAN bRestartQuery);

typedef NTSTATUS (*ZWQUERYDIRECTORYFILE)(
                                         HANDLE hFile,
                                         HANDLE hEvent,
                                         PIO_APC_ROUTINE IoApcRoutine,
                                         PVOID IoApcContext,
                                         PIO_STATUS_BLOCK pIoStatusBlock,
                                         PVOID FileInformationBuffer,
                                         ULONG FileInformationBufferLength,
                                         FILE_INFORMATION_CLASS FileInfoClass,
                                         BOOLEAN bReturnOnlyOneEntry,
                                         PUNICODE_STRING PathMask,
                                         BOOLEAN bRestartQuery
                                         );





NTSYSAPI NTSTATUS NTAPI ZwCreateFile(OUT PHANDLE FileHandle, IN ACCESS_MASK DesiredAccess, IN POBJECT_ATTRIBUTES ObjectAttributes, OUT PIO_STATUS_BLOCK IoStatusBlock, IN PLARGE_INTEGER AllocationSize OPTIONAL, IN ULONG FileAttributes, IN ULONG ShareAccess, IN ULONG CreateDisposition, IN ULONG CreateOptions, IN PVOID EaBuffer OPTIONAL, IN ULONG EaLength ); 
typedef NTSTATUS (*ZWCREATEFILE)(OUT PHANDLE FileHandle, IN ACCESS_MASK DesiredAccess, IN POBJECT_ATTRIBUTES ObjectAttributes, OUT PIO_STATUS_BLOCK IoStatusBlock, IN PLARGE_INTEGER AllocationSize OPTIONAL, IN ULONG FileAttributes, IN ULONG ShareAccess, IN ULONG CreateDisposition, IN ULONG CreateOptions, IN PVOID EaBuffer OPTIONAL, IN ULONG EaLength ); 

NTSYSAPI NTSTATUS NTAPI NtQueryInformationFile(IN HANDLE FileHandle, OUT PIO_STATUS_BLOCK IoStatusBlock, OUT PVOID FileInformation, IN ULONG Length, IN FILE_INFORMATION_CLASS FileInformationClass ); 
typedef NTSTATUS  (*NTQUERYINFORMATIONFILE)(IN HANDLE FileHandle, OUT PIO_STATUS_BLOCK IoStatusBlock, OUT PVOID FileInformation, IN ULONG Length, IN FILE_INFORMATION_CLASS FileInformationClass ); 

/////////////////////////////////////////////////////////////
//////////Global Data////////////////////////////////////////
UNICODE_STRING szDrvRegInfo;
UNICODE_STRING szDrvName;
ULONG gProcessNameOffset;

ZWQUERYDIRECTORYFILE			OldZwQueryDirectoryFile;
ZWCREATEFILE					OldZwCreateFile;
NTQUERYINFORMATIONFILE			OldNtQueryInformationFile;

/*
* Length of process name (rounded up to next DWORD)
*/
#define PROCNAMELEN     20

/*
* Maximum length of NT process name
*/
#define NT_PROCNAMELEN  16

#define FileIdFullDirectoryInformation 38
#define FileIdBothDirectoryInformation 37

////////////////////////////////////////////////////////////
