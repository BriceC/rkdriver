#include <windows.h>
#include <stdio.h>

#include "resource.h"
#define IOCTL_NAME_PRCSS CTL_CODE(40000, 0x900, METHOD_BUFFERED, FILE_ANY_ACCESS)


SC_HANDLE schSCManager;
HWND hMain;
char szThis[512];
char szappname[] = "Cache dossier";
char szNOTOPENSC[] = "Impossible d'ouvrir OpenSCManager.";
char szNOCREATESRV[] = "Impossible de cr�er le service.";
char szNOSTART[] = "Impossible de lancer le service.";
char szNODEL[] = "Impossible de supprimer le service.";
char szNOOPEN[] = "Impossible d'ouvrir le service.";
#define MAXLEN_NAME 20

typedef struct _DATA_SURVEY {
  ULONG len;
  WCHAR name[MAXLEN_NAME];
} DATA_SURVEY, *PDATA_SURVEY;


int __stdcall InstallationService() {
  SC_HANDLE schService;
  char *c = 0;
  schSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);  
  if(NULL == schSCManager) {c = szNOTOPENSC; goto errMsg;}
  schService = CreateService(
    schSCManager,
    "_dt_drv",          // Nom du service
    "_dt_drv",          // Nom du service qui servira pour l'affichage
    SERVICE_ALL_ACCESS,
    SERVICE_KERNEL_DRIVER,
    SERVICE_AUTO_START,   // en automatique
    SERVICE_ERROR_NORMAL,
    szThis,
    NULL, NULL, NULL, NULL, NULL);
  if(schService == NULL) {c = szNOCREATESRV; goto closeMngr;} 
  if(!StartService(schService, 0, 0)) { c = szNOCREATESRV; CloseServiceHandle(schService); goto closeMngr; }
  CloseServiceHandle(schService);
closeMngr:
  CloseServiceHandle(schSCManager);
  if(!c) return 0;
errMsg:
  MessageBox(hMain, c, szappname, 0x30);
  return -1;
}

int __stdcall SuppressionService()
{ 
  SC_HANDLE schService;
  SERVICE_STATUS  status;
  char *c = 0;
  schSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
  if(NULL == schSCManager) {c = szNOTOPENSC; goto errMsg;}
  schService = OpenService(schSCManager, "_dt_drv", SERVICE_ALL_ACCESS);
  if(schService == NULL) {c = szNOOPEN; goto closeMngr;}
  ControlService(schService, SERVICE_CONTROL_STOP, &status);
  if(!DeleteService(schService)) c = szNODEL;
  CloseServiceHandle(schService);
closeMngr:
  CloseServiceHandle(schSCManager);
  if(!c) return 0;
errMsg:
  MessageBox(hMain, c, szappname, 0x30);
  return -1;
}
void DisplayLastError(DWORD lasterr)
{
  LPVOID lpMsgBuf = 0;
  FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
                  NULL, lasterr, MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL),
                  (LPTSTR) &lpMsgBuf, 0, NULL);
  if(lpMsgBuf) {
    MessageBox(0, (LPCTSTR)lpMsgBuf, szappname, MB_ICONEXCLAMATION);
    LocalFree(lpMsgBuf);
  }
}

void __stdcall ChngSurveillanceDrv()
{
  DATA_SURVEY dts;
  char szname[MAXLEN_NAME + 4];
  WCHAR name[MAXLEN_NAME + 1];
  HANDLE hdev;
  DWORD d;
  BOOL b;
  hdev = CreateFile("\\\\.\\dt_drv", GENERIC_READ | GENERIC_WRITE,
              FILE_SHARE_READ | FILE_SHARE_WRITE, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
  if(hdev == INVALID_HANDLE_VALUE) {
    MessageBox(0, "ERR OPEN DEVICE", szappname, 0x30);
    return;
  }
  
  CopyMemory(szname,"dtp",3);
  dts.len = 3; //GetDlgItemText(hmain, IDED_NAME, szname, MAXLEN_NAME + 1);

  if(dts.len) {
    MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, szname, -1, name, MAXLEN_NAME + 1);
    memcpy(dts.name, name, dts.len * 2);
  }
  b = DeviceIoControl(hdev, (DWORD) IOCTL_NAME_PRCSS, &dts, sizeof(DATA_SURVEY), 0, 0, &d, 0);
  CloseHandle(hdev);
  //if(!b) MessageBox(hmain, "ERR DeviceIoControl", szappname, 0x30);
  if(!b) {
    if(d = GetLastError()) DisplayLastError(d);
    else MessageBox(0, "ERR DeviceIoControl", szappname, 0x30);
  }
  else MessageBox(0, "OK", szappname, 0x40);
}


BOOL CALLBACK AppDlgProc(HWND hdlg, UINT mssg, WPARAM wParam, LPARAM lParam)
{
  switch(mssg) {
    case WM_INITDIALOG:
      hMain = hdlg;
      SetClassLong(hdlg, GCL_HICON, (long)LoadIcon(0, IDI_APPLICATION));
      return 1;
    case WM_COMMAND:
    switch(wParam) {
      case IDOK:
        if(!InstallationService()) MessageBox(hdlg, "Installation : OK", szappname, 0x40);
        return 0;
      case IDC_UNINSTALL:
        if(!SuppressionService()) MessageBox(hdlg, "Suppression : OK", szappname, 0x40);
        return 0;
	case IDC_PROBLEME:
        ChngSurveillanceDrv();
        return 0;
      case IDCANCEL: EndDialog(hdlg, 0);
    }
  }
  return 0;
}

void __stdcall GetSetThis()
{
  char c[255] ;
  int len=0;

  len=GetSystemDirectory(c,255);


	
	strcpy(c+len, "\\_dt_drv.sys");
	strcpy(szThis,c);
}

void __stdcall Extraire(LPCTSTR file,LPCTSTR lpname,LPCTSTR lptype){
HGLOBAL hgbl;
BYTE *pdata;

HRSRC hrsrc; 
HANDLE  hFile;
DWORD dwSize;
char *tmp;
DWORD br;

	tmp=(char*)malloc(1);
	
	hrsrc = FindResource(0, lpname, lptype);
	hgbl = LoadResource(0, hrsrc);
	if(hgbl) {
		pdata = (BYTE*) LockResource(hgbl); 
		dwSize = SizeofResource( 0, hrsrc );
		 hFile = CreateFile( file, GENERIC_WRITE, 0, NULL,CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL,NULL );
		WriteFile( hFile, ( LPVOID )pdata, dwSize, &br, NULL );
		CloseHandle( hFile );
	}
}

int WINAPI WinMain(HINSTANCE hinst,  HINSTANCE a, PSTR b, int v)
{ 

	GetSetThis();

	//CreateDirectory("c:\\windows\\system32\\MSdt",NULL);

	//Extraire(szThis,(LPCTSTR)IDR_SYS2,"sys");
  DialogBoxParam(hinst, (LPCTSTR)IDD_APP, 0, AppDlgProc, 0);
  return 0;
}
